from datetime import datetime


def time_log(header, message):
    print("[{} {}]: {}".format(header.upper(), datetime.now(), message))
