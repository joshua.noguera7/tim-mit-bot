# Tim Bot (Updated 7/21/19)
## About
Tim Bot is a custom discord bot in the MIT Class of 2023 discord server.
## Features
### Verification
The MIT Class of 2023 discord guild is only for MIT Students. To ensure that only MIT Students enter the main channels, Tim Bot automatically verifies members by ensuring they have access to a Kerberos account.
### Roles
Tim Bot also provides easy access for members to change their course roles or pronoun roles by adding or removing reactions from a message.
## Creation
Tim Bot is maintained by Joshua Noguera '23 (Josh_Player1#4000)
Contributions are welcome! Message me if you want to get involved.