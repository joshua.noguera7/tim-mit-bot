import discord
import timBotSecrets
import asyncio
import shelve
import imapclient
import pyzmail
import re
from jLib import *
import config

time_log("startup", "Libraries loaded!")


# Generic save function for shelve
def save(data, key):
    with shelve.open("tim_data") as s:
        s[key] = data
        time_log("Save", "Saved {} in {} data base.".format(data, key))


# Load persistent variables
with shelve.open("tim_data") as w:
    # Loads the ID for the role that verifies someone as an MIT student
    global MITStudentRoleID
    if "MITStudentRoleID" in w.keys():
        MITStudentRoleID = w["MITStudentRoleID"]
        time_log("Load", "Successfully loaded the \"MIT Student Role\" ID")
    else:
        time_log("error", '"MIT Student Role ID" is not found!')
        MITStudentRoleID = int(input("Please input the id of the MIT student role ID: "))
        w["MITStudentRoleID"] = MITStudentRoleID
        time_log("Save", "Successfully saved the \"MIT Student Role\" ID")
    # Loads the ID for the role that has elevated privileges for Tim Bot
    global botManagerRoleID
    if "botManagerRoleID" in w.keys():
        botManagerRoleID = w["botManagerRoleID"]
        time_log("Load", "Successfully loaded the \"Bot Manager Role\" ID")
    else:
        time_log("error", '"Bot Manager Role ID" is not found!')
        botManagerRoleID = int(input("Please input the id of the Bot Manager role ID: "))
        w["botManagerRoleID"] = botManagerRoleID
        time_log("Save", "Successfully saved the \"Bot Manager Role\" ID")
    global usedEmails
    if "usedEmails" in w.keys():
        usedEmails = w["usedEmails"]
        time_log("Load", "Successfully loaded the used emails list")
    else:
        time_log("error", "Used emails list not found! Continuing with an empty list...")
        usedEmails = []
        w["usedEmails"] = usedEmails

tim = discord.Client()


# Things to be run once at when the bot starts up and is ready
async def on_ready():  # TODO: Fix why the strings are not being recognized
    await tim.wait_until_ready()
    time_log("startup", "Tim bot is ready!")
    roles_channel = discord.utils.get(tim.get_all_channels(), id=config.roleChannelID)  # Gets the channel that holds the messages to get roles
    role_messages = await roles_channel.history(limit=100).flatten()  # Actually retrieves messages
    for message in role_messages:  # Iterates through the retrieved messages
        if message.id in config.messageIDEmojiDict.keys():  # Checks if a message is one that should have reactions
            reactions = message.reactions  # Gets the reactions of the message
            emoji_list = []  # Blank list to hold emojies of the reactions of the message
            for reaction in reactions:
                emoji_list.append(str(reaction))  # Populates the list with the reactions of the message
            for string_emoji in config.messageIDEmojiDict[message.id].keys():
                if string_emoji not in emoji_list:  # If the message does not contain the required reactions for said message, it will add them.
                    await message.add_reaction(string_emoji)
                    await asyncio.sleep(0.5)  # Waits a bit at the end to make sure they're in order.


# Verification emails are read and processed every X seconds.
async def read_emails():
    await tim.wait_until_ready()  # Waits until the bot is ready because of discord object lookups
    while True:
        time_log("verification", "Checking for emails...")
        # Time out occur, relogging is done via try-except
        try:
            imapObj = imapclient.IMAPClient('imap.gmail.com', ssl=True)
            imapObj.select_folder('INBOX', readonly=False)  # All emails should arrive in inbox
            unreadEmailUUIDs = imapObj.search(["UNSEEN"])  # Read emails are already processed
        except:
            imapObj = imapclient.IMAPClient('imap.gmail.com', ssl=True)
            imapObj.login(timBotSecrets.tbeaverbotEmail, timBotSecrets.tbeaverbotPassword)  # Actually logs in
            imapObj.select_folder('INBOX', readonly=False)
            unreadEmailUUIDs = imapObj.search(["UNSEEN"])
        if len(unreadEmailUUIDs) > 0:  # If there are unread emails...
            time_log("verification", "{} Email(s) received".format(len(unreadEmailUUIDs)))
            raw_emails = imapObj.fetch(unreadEmailUUIDs, ["BODY[]"])  # Retrieves the data for the emails and marks them
            # as read
            interpretedMessages = []
            for raw_email in raw_emails.values():
                interpretedMessages.append(pyzmail.PyzMessage.factory(raw_email[b'BODY[]']))  # Interprets the data and
                # returns a Pyzmail object with parsed information
            messagesFromMITEmail = []
            for message in interpretedMessages:
                if message.get_address("from")[1].split("@")[-1] in ("mit.edu", "exchange.mit.edu"):  # Verification
                    # should only work if the email comes from an @mit one.
                    messagesFromMITEmail.append(message)
            time_log("verification", "{} MIT Email(s) Found".format(len(messagesFromMITEmail)))
            MITGuild = discord.utils.get(tim.guilds, id=523718048612614164)  # Returns the discord object that
            # represents the server
            for message in messagesFromMITEmail:
                content = str(message.text_part.get_payload().strip())[2:-1]  # The [2:-1] is to remove head and tail
                # string objects from conversion from bytes object
                fromEmail = message.get_address("from")[1]  # [1] is here because [0] includes the literal name of the
                # sender, nor address. This could be an issue that needs checking later.
                time_log("verification", "Verifying email from: {}, with content: {}"
                         .format(fromEmail, content))
                username = re.search(r"(^[^#]{2,32})#(\d{4})", content)  # The pattern here matches discord username
                # like example#0001
                if username is not None:
                    time_log('verification', "Regex pattern found: {}".format(username.string))
                    member = discord.utils.get(MITGuild.members, name=username.group(1),
                                               discriminator=username.group(2))
                    if member is None:
                        content = content.replace("?", "")  # This section is very similar, sometimes in decoding there
                        # are random "?", this removes them and attempts again
                        username = re.search(r"(^[^#]{2,32})#(\d{4})", content)
                        member = discord.utils.get(MITGuild.members, name=username.group(1),
                                                   discriminator=username.group(2))
                    if member is not None:
                        time_log("verification", "Found member {} in MIT Discord".format(member.display_name))
                        if str(fromEmail).split("@")[0] not in usedEmails:
                            time_log("verification", '"{}" has not been used before, completing verification '
                                                     'for user: {}'.format(fromEmail, member.display_name))
                            MITStudentRole = discord.utils.get(MITGuild.roles, id=MITStudentRoleID)
                            if MITStudentRole not in member.roles:
                                await member.add_roles(MITStudentRole)
                            usedEmails.append(str(fromEmail).split("@")[0])
                            save(usedEmails, "usedEmails")
                            await member.send("You've been successfully verified! "
                                              "Welcome to the MIT'23 discord server!")
                            time_log("verification", "Completed verification for: {}".format(member.display_name))
                        else:
                            time_log("error: verification", "Email has already been used!")
                    else:
                        time_log("error: verification", "No user found.")
                else:
                    time_log("error: verification", "No regex pattern found.")
        else:
            time_log("verification", "No emails found.")
        await asyncio.sleep(120)  # How frequent the emails are read and processed


@tim.event
async def on_member_join(member):
    await discord.utils.get(tim.get_all_channels(), id=523925482366828564)\
        .send("Welcome {} to the MIT'23 discord server! Please read {}. If you are a guest, please"
              " @moderators for the guest role.".format(member.mention, discord.utils.get(
                                                            tim.get_all_channels(), id=582722845549789205).mention))


@tim.event  # This block triggers when a message is sent
async def on_message(message):
    global botManagerRoleID
    if message.guild == await tim.fetch_guild(523718048612614164):  # TODO: All this is messy and should be redone
        try:
            if (message.author.id == 156942083788242944) or \
                    (discord.utils.get(message.guild.roles, id=botManagerRoleID) in message.author.roles):
                if message.content.startswith("!send"):
                    args = message.content.split()
                    await discord.utils.get(tim.get_all_channels(), id=int(args[1])).send(" ".join(args[2:]))
                if message.content.startswith("!set"):
                    args = message.content.split()
                    if args[0] == "!set":
                        global MITStudentRoleID
                        if args[1] == "MITStudentRoleID":
                            MITStudentRoleID = int(args[2])
                            save(MITStudentRoleID, "MITStudentRoleID")
                        elif args[1] == "botManagerRoleID":
                            botManagerRoleID = int(args[2])
                            save(botManagerRoleID, "botManagerRoleID")
                    elif args[0] == "!refresh_email_db":
                        # TODO: Call to a task that goes though all emails and refreshes the database of used emails
                        #  and verified members
                        pass
                    elif args[0] == "!verify":
                        # TODO: Add a function that manually verifies members from the discord environment and stores
                        #  their data
                        pass
        except AttributeError:
            pass
    if message.content == "!ping":
        await message.channel.send("Pong!")


@tim.event
async def on_raw_reaction_add(payload):
    if payload.message_id in config.messageIDEmojiDict:  # If the message is one to be checked for as defined in config.py
        MITGuild = discord.utils.get(tim.guilds, id=config.MITGuildID)
        reaction_member = discord.utils.get(MITGuild.members, id=payload.user_id)
        addRole = discord.utils.get(MITGuild.roles, id=config.messageIDEmojiDict[payload.message_id][payload.emoji.name])  # Past 3 lines get relavant information about the reaction
        await reaction_member.add_roles(addRole, reason="Added reaction to message in #roles", atomic=True)  # Adds the role


@tim.event
async def on_raw_reaction_remove(payload):  # Same s on_raw_reaction_add event, except removes the role
    if payload.message_id in config.messageIDEmojiDict:
        MITGuild = discord.utils.get(tim.guilds, id=config.MITGuildID)
        reaction_member = discord.utils.get(MITGuild.members, id = payload.user_id)
        removeRole = discord.utils.get(MITGuild.roles, id = config.messageIDEmojiDict[payload.message_id][payload.emoji.name])
        await reaction_member.remove_roles(removeRole, reason="Removed reaction to message in #roles", atomic=True)


# Initialize bot and tasks
tim.loop.create_task(on_ready())
tim.loop.create_task(read_emails())
tim.run(timBotSecrets.client_token)
